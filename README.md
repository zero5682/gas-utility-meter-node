#Requirements

## Redis
## RabbitMq
## SocketIO
## node_modules


#Installing

Install all node_modules to server and front folder 

npm install --verbose


# Running server

node index.js


# Running front

npm start


# Running tests

npm test


# Running simulation

When running server then simulation will start, 
and every 10 second creates new data and front side chart will refresh.
