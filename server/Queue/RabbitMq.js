const QUEUE = 'gas_utility';
const URL = 'amqp://localhost';
const amqp = require('amqplib');
const PubSub = require('pubsub-js');

class RabbitMQ 
{

  constructor() 
  {
    this.connection = null
    this.channel = null
  }

  connect = async () => 
  {
      try {
        this.connection = await amqp.connect(URL, "heartbeat=60");
        console.log("RabbitMQ Running...");
        this.channel = await this.connection.createChannel();
        console.log("RabbitMQ Channel created...");
        await this.channel.assertQueue(QUEUE, {durable: true});

      } catch (err){
          console.log(err)
          throw new Error('Connection failed')
      }
  }

  post = async (data) => 
  {
      if (!this.connection) await this.connect()
      try {
          this.channel.sendToQueue(QUEUE, new Buffer.from(JSON.stringify(data)), {durable: true});
          console.log("Data posted")
      } catch (err){
          console.error(err) 
      }
  }
  recieve = async (socketClient) => 
  {
    if (!this.connection) await this.connect();
    this.channel.consume(QUEUE, (msg) => {
      console.log("Socket -> ", socketClient.id)
      PubSub.publish("update-socket-chart", {data: msg.content.toString()});
      console.log("RabbitMQ -> published -> ", msg.content.toString());
      this.channel.ack(msg);
    });
  }
}
module.exports = RabbitMQ