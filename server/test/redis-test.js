const assert = require('chai').assert;
const Gas = require("../Model/Gas");
const redis = require("redis-mock");
const client = redis.createClient();

describe("Redis", function() {
    it("Add JSON data", function() {

        const key = "gas";
        const data = [
            new Gas(12),
            new Gas(13)
        ];

        client.set(key, JSON.stringify(data), () => {
            client.get(key, (err, result) => {
                if (err) {
                    assert.isTrue(false);
                    return;
                }
                const redisData = JSON.parse(result);
                assert.isTrue(redisData.length > 0 && redisData.length == 2);
            });
        });
    });

    it("Append data", function() {

        const key = "gas";
        client.get(key, (err, result) => {
            if (err) {
                assert.isTrue(false);
                return;
            }
            const redisData = JSON.parse(result);
            assert.isTrue(redisData.length > 0 && redisData.length == 2);

            redisData.push(new Gas(13));

            client.set(key, JSON.stringify(redisData), () => {
                client.get(key, (err, result) => {
                    if (err) {
                        assert.isTrue(false);
                        return;
                    }
                    const redisData = JSON.parse(result);
                    assert.isTrue(redisData.length > 0 && redisData.length == 3);
                });
            });

        });
    });
});