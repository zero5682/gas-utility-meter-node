const redis = require("async-redis");
const client = redis.createClient();

client.on("error", (error) => {
  console.error(error);
});

client.on("ready", () => {
    console.log("Redis is ready...");
})

class RedisAdapter 
{
    Add = async (key, data) => 
    {
        try 
        {
            await client.set(key, data);
        } 
        catch(ex) 
        {
            throw new Error("Failed to add data");
        }
    }

    Remove = async (key) => 
    {
        try 
        {
            await client.del(key);
        }
        catch(ex) 
        {
            throw new Error("Failed to remove: " + key);
        }
    }

    GetAll = async (key) => 
    {
        try 
        {
            return await client.get(key);
        }
        catch(ex) 
        {
            throw new Error("Failed to get all data");
        }    
    }

}

module.exports = RedisAdapter