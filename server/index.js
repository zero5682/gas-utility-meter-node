const RabbitMQ = require("./Queue/RabbitMq");
const GasService = require("./Service/GasService");

const cors = require('cors');
const PubSub = require('pubsub-js');

const rabbitMq = new RabbitMQ();
const gasService = new GasService(rabbitMq);

let simulationRunning = false;
const port = 3600

var http = require("http").createServer(), 
    io = require("socket.io")(http, {
      cors: cors,
      path: "/",  
      serveClient: true,
    });

http.listen(port);


io.on('connection', async (client) => {

  await rabbitMq.recieve(client);
  await gasService.RemoveAll();

  PubSub.unsubscribe("update-socket-chart");
  console.log("SocketIO client -> ", client.id);
  client.on('update-chart-request', async (data) => { 
    const gasAll = await gasService.GetAll();
    client.emit('update-chart-response', {data: gasAll})
  });

  client.on('post-gas-amount', async (data) => { 
    await gasService.Add(data);
  });
  
  
  client.on('disconnect', () => { 
    console.log("Disconnected"); 
    simulationRunning = false;
    PubSub.unsubscribe("update-socket-chart");
    gasService.stopSimulation();
  
  });
  PubSub.subscribe("update-socket-chart", (msg, gasAll) => {
    client.emit('update-chart-response', {data: gasAll.data})
  });

  if (!simulationRunning) {
    gasService.Simulate();
    simulationRunning = true;
  }
});