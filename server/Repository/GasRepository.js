const Gas = require("../Model/Gas");

class GasRepository 
{
    constructor(redis) 
    {
        this.redis = redis;
        this.key = "gas";
    }

    Add = async (data) => 
    {
        let gasData = await this.redis.GetAll(this.key);
        let gasDataJson = gasData == null ? [] : JSON.parse(gasData);

        gasDataJson.push(new Gas(data));
        return await this.redis.Add(this.key, JSON.stringify(gasDataJson));
    }

    Remove = async () => 
    {
        return await this.redis.Remove(this.key);
    }

    GetAll = async () => 
    {
        return await this.redis.GetAll(this.key);
    }

}

module.exports = GasRepository