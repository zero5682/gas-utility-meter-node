class Gas 
{
    constructor(amount) 
    {
        this.amount = amount;
        this.id = Math.floor(Math.random()*10) + 1;
        this.timestamp = Math.floor(Date.now() / 1000);
    }
}

module.exports = Gas