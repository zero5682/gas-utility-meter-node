
const Gas = require("../Model/Gas");
const Logger = require("../Logger/Logger");
const RedisAdapter = require("../Database/RedisAdapter");
const GasRepository = require("../repository/GasRepository");

class GasService 
{

    constructor(rabbitMq) 
    {
        this.rabbitMq = rabbitMq;
        this.logger = new Logger();
        this.gasRepository = new GasRepository(new RedisAdapter());
    }

    GetAll = async () => 
    {
        try 
        {
            return await this.gasRepository.GetAll();
        } 
        catch(ex) 
        {
            this.logger.Log(ex)
        }
    }

    Add = async (data) => 
    {
        try 
        {
            await this.gasRepository.Add(new Gas(parseInt(data.gas)))
            const gasJson = JSON.parse(await this.gasRepository.GetAll());
            await this.rabbitMq.post(gasJson);
        }
        catch(ex) 
        {
            this.logger.Log(ex);
        }
    }

    Simulate = () => 
    {
        console.log("Simulating...");
        this.simulation = setInterval(async () => {
            console.log("Simulating .. running ...");
            await this.gasRepository.Add(Math.floor(Math.random() * 30));
            await this.rabbitMq.post(JSON.parse(await this.gasRepository.GetAll()));
        }, 10000);
    }

    stopSimulation = () => {
        clearTimeout(this.simulation);
    }

    RemoveAll = async () => 
    {
        try 
        {
            await this.gasRepository.Remove();
        } 
        catch(ex) 
        {
            this.logger.Log(ex);
        }
    }
}

module.exports = GasService