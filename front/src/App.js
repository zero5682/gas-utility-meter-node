import {useState, useEffect} from "react";
import GasChart from './GasChart';
import './App.css';

const io = require("socket.io-client");

const socket = io(`http://localhost:3600`, {
  reconnection: true,
  secure: true,
  transports: ["websocket"]
});

socket.on("connect", () => {  console.log("Socket connected..."); });
socket.io.on("error", (error) => { console.log("Socket error: ", error) });

const App = () => {

  const [gasAmount, setGasAmount] = useState(0)
  const [gas, setGas] = useState(null)
  
  useEffect(() => {
      socket.on('update-chart-response', gas => {
          setGas(null);
          setGas(JSON.parse(gas.data));
      })

      socket.emit("update-chart-request");
      return () => {}
  }, [])

  const postGasAmount = () => {
    socket.emit("post-gas-amount", {gas: gasAmount});
  }

  return (
    <div className="App">
      <div className="chart-input-container">
        <div className="chart-input-left">
          Kogus:
        </div>
        <div className="chart-input-right">
          <input type="number" value={gasAmount} onChange={ev => setGasAmount(ev.target.value)} />
          <button onClick={() => postGasAmount()}>Lisa</button>
        </div>
      </div>
      <div className="chart-container">
        { gas != null ? <GasChart gas={gas} /> : null }
      </div>
    </div>
  );
}

export default App;
