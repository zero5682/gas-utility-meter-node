import React from 'react'
import { Chart } from 'react-charts'

const GasChart = (props) => {
    const data = React.useMemo(() => [
        {
          label: 'Kogus',
          data: props.gas.map((item, index) => {
            if (item.amount == null) return undefined;
            return [
              index, item.amount.amount == null ? item.amount : item.amount.amount
            ]
          }).filter(x => x != undefined)
        },
      ],[]
    )
   
    const axes = React.useMemo(() => [
        { primary: true, type: 'linear', position: 'bottom' },
        { type: 'linear', position: 'left' }
      ],[]
    )
   
    return (
      <div
        style={{
          width: '90vw',
          height: '450px',
          margin: "0 auto"
        }}
      >
        <Chart data={data} axes={axes} />
      </div>
    )
  }

export default GasChart;  